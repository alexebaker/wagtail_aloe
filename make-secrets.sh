#!/usr/bin/env bash


cat /dev/urandom | strings | grep -oE '[a-zA-Z0-9!@#$%^&*?+]' | head -n 32 | tr -d '\n' > .db_password.txt
cat /dev/urandom | strings | grep -oE '[a-zA-Z0-9!@#$%^&*?+]' | head -n 64 | tr -d '\n' > .secret_key.txt

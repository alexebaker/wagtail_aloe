from .base import *

DEBUG = False

STATIC_URL = BASE_URL + '/static/'
MEDIA_URL = BASE_URL + '/media/'

try:
    from .local import *
except ImportError:
    pass

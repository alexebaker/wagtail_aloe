#!/usr/bin/env bash


crond

if [[ ! -z "$TZ" ]] ; then
    echo "$TZ" > /etc/timezone
    rm /etc/localtime
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
fi

addgroup -g $GROUP_ID -S $USER_NAME
adduser -u $USER_ID -s /sbin/nologin -G $USER_NAME -H -S $USER_NAME

export ROOT_PASSWORD=$(cat /dev/urandom | strings | grep -oE '[a-zA-Z0-9!@#$%^&*()?+]' | head -n 32 | tr -d '\n')
echo -e "$ROOT_PASSWORD\n$ROOT_PASSWORD" | passwd root >> /dev/null 2>&1

export USER_PASSWORD=$(cat /dev/urandom | strings | grep -oE '[a-zA-Z0-9!@#$%^&*()?+]' | head -n 32 | tr -d '\n')
echo -e "$USER_PASSWORD\n$USER_PASSWORD" | passwd $USER_NAME >> /dev/null 2>&1

unset ROOT_PASSWORD WAGTAIL_PASSWORD

if [[ -f /config/keys/letsencrypt/fullchain.pem && -f /config/keys/letsencrypt/privkey.pem ]] ; then
    cp /config/keys/letsencrypt/fullchain.pem /etc/nginx/certs/fullchain.pem
    cp /config/keys/letsencrypt/privkey.pem /etc/nginx/certs/privkey.pem
else
    openssl req -x509 -newkey rsa:4096 -keyout /etc/nginx/certs/privkey.pem -out /etc/nginx/certs/fullchain.pem -days 36500 -nodes -subj "/CN=$URL"
fi

if [[ -f /config/nginx/dhparams.pem && -s /config/nginx/dhparams.pem ]] ; then
    cp /config/nginx/dhparams.pem /etc/nginx/certs/dhparams.pem
fi

chmod 644 /etc/nginx/certs/fullchain.pem
chmod 600 /etc/nginx/certs/privkey.pem
chmod 600 /etc/nginx/certs/dhparams.pem

export CPU_COUNT=$(grep -c ^processor /proc/cpuinfo)

envsubst '${USER_NAME} ${CPU_COUNT}' < /etc/nginx/nginx.tmpl.conf > /etc/nginx/nginx.conf

for f in $(ls /etc/nginx/conf.tmpl.d/*.conf) ; do
    envsubst '${URL} ${WEB_ROOT}' < $f > /etc/nginx/conf.d/$(basename $f)
done

if [[ "$DEPLOY_ENV" == "Prod" ]] ; then
     sed -i '/add_header Strict-Transport-Security/s/#//g' /etc/nginx/nginx.conf
     sed -i '/ssl_stapling/s/#//g' /etc/nginx/nginx.conf
     sed -i '/ssl_stapling_verify/s/#//g' /etc/nginx/nginx.conf
fi

mkdir -p $WEB_ROOT
chown -R $USER_ID:$GROUP_ID $WEB_ROOT

exec "$@"

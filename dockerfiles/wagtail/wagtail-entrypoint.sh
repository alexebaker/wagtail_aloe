#!/usr/bin/env bash


if [[ ! -z "$TZ" ]] ; then
    echo "$TZ" > /etc/timezone
    rm /etc/localtime
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
fi

addgroup -g $GROUP_ID -S $USER_NAME
adduser -u $USER_ID -s /sbin/nologin -G $USER_NAME -H -S $USER_NAME

export ROOT_PASSWORD=$(cat /dev/urandom | strings | grep -oE '[a-zA-Z0-9!@#$%^&*()?+]' | head -n 32 | tr -d '\n')
echo -e "$ROOT_PASSWORD\n$ROOT_PASSWORD" | passwd root >> /dev/null 2>&1

export USER_PASSWORD=$(cat /dev/urandom | strings | grep -oE '[a-zA-Z0-9!@#$%^&*()?+]' | head -n 32 | tr -d '\n')
echo -e "$USER_PASSWORD\n$USER_PASSWORD" | passwd $USER_NAME >> /dev/null 2>&1

unset ROOT_PASSWORD WAGTAIL_PASSWORD

pip install -U -r /code/requirements.txt

python manage.py migrate
python manage.py collectstatic -c --no-input

chown -R $USER_ID:$GROUP_ID /code
chown -R $USER_ID:$GROUP_ID /code/.staticfiles || true
chown -R $USER_ID:$GROUP_ID /code/.mediafiles || true

export CPU_COUNT=$(python -c "import os; print(os.cpu_count())")

if [[ $DJANGO_ENV == "Dev" ]] ; then
    set -- python manage.py runserver 0.0.0.0:8000
else
    set -- gunicorn aloe.wsgi --bind 0.0.0.0:8000 --workers $CPU_COUNT --worker-class gevent
fi

su-exec $USER_NAME "$@"

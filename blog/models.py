from django.db import models

from wagtail.core import blocks
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.search import index


class BlogIndexPage(Page):
    subpage_types = [
        'blog.BlogPage'
    ]

    def get_context(self, request):
        context = super().get_context(request)

        pages = self.get_children().live().order_by('-blogpage__pub_date')
        featured_pages = [p for p in pages if p.specific.is_featured]
        pages = [p for p in pages if not p.specific.is_featured]

        other_pages = []
        while len(pages) > 0:
            if len(pages) == 1:
                other_pages.append((pages[0], None))
                pages = pages[1:]
            else:
                other_pages.append((pages[0], pages[1]))
                pages = pages[2:]

        context['featured_posts'] = featured_pages
        context['other_posts'] = other_pages
        return context


class BlogPage(Page):
    subpage_types = []

    pub_date = models.DateField("Publish date")
    is_featured = models.BooleanField(default=False)
    main_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    body = StreamField([
        ('heading', blocks.CharBlock()),
        ('paragraph', blocks.RichTextBlock()),
        ('image', ImageChooserBlock()),
    ])

    content_panels = Page.content_panels + [
        FieldPanel('pub_date'),
        FieldPanel('is_featured'),
        ImageChooserPanel('main_image'),
        StreamFieldPanel('body')
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

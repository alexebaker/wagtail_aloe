from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.search import index


class AboutPage(Page):
    subpage_types = []

    body = RichTextField(blank=True)
    about_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    content_panels = Page.content_panels + [
        ImageChooserPanel('about_image'),
        FieldPanel('body')
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]
